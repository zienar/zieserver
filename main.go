package main

import (
	"log"
	"net/http"
	"zienar/routes"
	"zienar/utils"
	"zienar/data"
	"github.com/pelletier/go-toml"
	"github.com/rs/cors"
)

type App struct {
	Rest *routes.Router
}

/*
*The first level routing logic
*Checks the main route that may have
*a sub-route
 */
func (h *App) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	var head string
	head, req.URL.Path = utils.ShiftPath(req.URL.Path)

	switch head {

	case "auth":
		h.Rest.Auth.ServeHTTP(res, req)

	default:
		http.Error(res, "Not Found", http.StatusNotFound)
		log.Println("1st level route not found")
	}

	return

}

func main() {

	config, ok := toml.LoadFile("zienar.toml")

	if ok != nil {
		log.Fatal(ok)
	}


	rcfg := data.RedisConfig{
		Protocl: config.Get("Redis.protocol").(string),
		Addr: config.Get("Redis.address").(string),
		Port: int(config.Get("Redis.port").(int64)),
		Poolsize: int(config.Get("Redis.poolsize").(int64)),
	}

	pcfg := data.PostgresConfig{
		Host: config.Get("Postgres.host").(string),
		Port: int(config.Get("Postgres.port").(int64)),
		User: config.Get("Postgres.user").(string),
		Key: config.Get("Postgres.key").(string),
		Dbname: config.Get("Postgres.database").(string),
	}	

	data.InitPostgres(&pcfg)
	data.InitRedis(&rcfg)
	data.InitJwt(config.Get("JWT.sig").(string))

	a := &App{
		Rest: new(routes.Router),
	}

	c := cors.Default() //enable cors

	err := http.ListenAndServe(":53768", c.Handler(a))

	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}
