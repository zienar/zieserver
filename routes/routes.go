package routes

type Router struct {
	Auth *AuthRoute
}

type requestS struct {
	User string `json:"Zid"`
	Key  string `json:"key"`
}

type authS struct {
	Token   string   `json:"token"`
	Request requestS `json:"request"`
}
