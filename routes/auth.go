package routes

import (
	"encoding/json"
	"log"
	"net/http"
	"zienar/data"
	"zienar/utils"
)

type AuthRoute struct {
}

func (a *AuthRoute) ServeHTTP(res http.ResponseWriter, req *http.Request) {

	// _, req.URL.Path = utils.ShiftPath(req.URL.Path)
	var head string

	if req.URL.Path != "/" {
		head, req.URL.Path = utils.ShiftPath(req.URL.Path)
		switch head {
		case "login":
			a.loginHandler().ServeHTTP(res, req)

		case "signup":
			a.signupHandler().ServeHTTP(res, req)

		case "reset":
			a.resetHandler().ServeHTTP(res, req)
		default:
			http.Error(res, "Not Found", http.StatusNotFound)
			log.Println("2nd level route not found")
		}
		return
	}
}

func (a *AuthRoute) loginHandler() http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		//handle login here
		var v authS
		var adId = "@zn_none"

		if req.Body == nil {
			http.Error(res, "Please send a request body", 400)
			log.Println("No Request Body")
			return
		}

		err := json.NewDecoder(req.Body).Decode(&v)

		if err != nil {
			http.Error(res, err.Error(), 400)
			log.Println(err)
			return
		}

		//Get login id and password from data
		//check for validity
		addinID, chkerr := data.AuthCheck(v.Request.User, v.Request.Key)

		if chkerr != nil {
			http.Error(res, err.Error(), 400)
			log.Println(err)
			return
		}

		if addinID != "" {
			adId = "@zn_" + addinID
		}

		token, err := utils.NewToken(v.Request.User+adId, data.Jsign())

		res.Header().Set("Content-Type", "application/json")
		json.NewEncoder(res).Encode(token)

	})
}

func (a *AuthRoute) signupHandler() http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {

		var v authS

		if req.Body == nil {
			http.Error(res, "Please send a request body", 400)
			log.Println("No Request Body")
			return
		}

		err := json.NewDecoder(req.Body).Decode(&v)

		if err != nil {
			http.Error(res, err.Error(), 400)
			log.Println(err)
			return
		}

		//securely encode key and salt for storage
		v.Request.Key, err = utils.Bake(v.Request.Key)

		if err != nil {
			log.Println(err)
			return
		}

		//send a jwt with addin id and login name
		token, err := utils.NewToken(v.Request.User+"@zn_none", data.Jsign())

		//generate a usrname of form : @zn_0pPKHjWprnVxGH7dEsAoXX2YQvU
		usrname := utils.DefaultUserName()

		ok := data.RegisterUser(v.Request.User, v.Request.Key, usrname)

		if ok != nil {
			http.Error(res, "User Exists", 400)
			log.Println(ok)
			return
		}

		res.Header().Set("Content-Type", "application/json")
		json.NewEncoder(res).Encode(token)

	})
}

func (a *AuthRoute) resetHandler() http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		//handle password reset here
	})
}
