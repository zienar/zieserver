module zienar

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/lib/pq v1.0.0
	github.com/mediocregopher/radix.v2 v0.0.0-20181115013041-b67df6e626f9
	github.com/pelletier/go-toml v1.2.0
	github.com/rs/cors v1.6.0
	github.com/segmentio/ksuid v1.0.2
	golang.org/x/crypto v0.0.0-20190103213133-ff983b9c42bc
	golang.org/x/sys v0.0.0-20190102155601-82a175fd1598 // indirect
)
