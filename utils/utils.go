package utils

import (
	"errors"
	"path"
	"strings"

	"github.com/segmentio/ksuid"
)

func ShiftPath(p string) (head, tail string) {

	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1

	if i <= 0 {
		return p[1:], "/"
	}

	return p[1:i], p[i:]
}

var (
	ErrInvalidHash         = errors.New("the encoded hash is not in the correct format")
	ErrIncompatibleVersion = errors.New("incompatible version of argon2")
)

//argon parameters: double check later
var Pargon *params = &params{
	memory:      64 * 1024,
	iterations:  3,
	parallelism: 2,
	saltLength:  16,
	keyLength:   32,
}

func Bake(key string) (encHash string, err error) {
	return generateFromPassword(key, Pargon)
}

func Liquefy(freshKey, encHash string) (matched bool, err error) {
	return comparePasswordAndHash(freshKey, encHash)
}

func DefaultUserName() string {
	return "@zn_" + ksuid.New().String()
}
