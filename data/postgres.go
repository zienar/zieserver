package data

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"zienar/utils"

	_ "github.com/lib/pq"
	//"strconv"
)

type PostgresConfig struct {
	Host string
	Port int
	User string
	Key  string // this field depends on postgres setup
	//insert password=%s and cfg.Key into Sprintf below if necessary
	Dbname string
}

var db *sql.DB

func InitPostgres(cfg *PostgresConfig) error {

	dbInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.User, cfg.Key, cfg.Dbname)

	var err error

	db, err = sql.Open("postgres", dbInfo)

	if err != nil {
		return err
	}

	err = db.Ping()

	if err != nil {
		return err
	}

	log.Println("Succesfully Init'd Postgres")
	return nil

}

func RegisterUser(loginId, key, username string) error {

	insertstr := `INSERT INTO public."Users" (login_id, passwd, username, avatar, cover_photo, about, addon_id)
                  VALUES ($1,$2, $3, NULL, NULL, NULL, NULL)
                `

	stmt, err := db.Prepare(insertstr)

	if err != nil {
		log.Println(err)
		return err
	}

	defer stmt.Close()

	_, ok := stmt.Exec(loginId, key, username)

	if ok != nil {
		return ok
	}

	return nil
}

func AuthCheck(userid, key string) (string, error) {

	var keyd, id string
	addinStr := ""
	var addin sql.NullString

	authSql := `SELECT login_id, passwd, addon_id FROM public."Users"
			  WHERE login_id=$1 `

	row := db.QueryRow(authSql, userid)

	ok := row.Scan(&id, &keyd, &addin)

	if ok == sql.ErrNoRows { //user doesn't exist
		return "", errors.New("User Does Not Exist")

	} else if ok != nil { //database error
		log.Fatal(ok)
	}

	if addin.Valid {
		addinStr = addin.String
	}

	match, err := utils.Liquefy(key, keyd) //actually check the provided credentials

	if err != nil { //decryption failed
		log.Fatal(err)
		//return "", err
	}

	if match == true {
		return addinStr, nil
	}

	return "", errors.New("Credentials Failed")

}
