package data

import (
	"log"
	"strconv"

	"github.com/mediocregopher/radix.v2/pool"
)

type JWtSign struct {
	Pphrase string
}

type RedisConfig struct {
	Protocl  string
	Addr     string
	Port     int
	Poolsize int
}

var redispool *pool.Pool
var sign *JWtSign

func InitJwt(sig string) {
	sign = &JWtSign{Pphrase: sig}
}

func Jsign() string {
	return sign.Pphrase
}

func InitRedis(cfg *RedisConfig) error {

	addr := cfg.Addr + ":" + strconv.Itoa(cfg.Port)
	var ok error

	redispool, ok = pool.New(cfg.Protocl, addr, cfg.Poolsize)

	if ok != nil {
		return ok
	}

	log.Println("Succesfully Init'd Redis")
	return nil
}

func command(cmd string, args ...interface{}) error {

	resp := redispool.Cmd(cmd, args)

	if resp.Err != nil {
		return resp.Err
	}

	return nil
}
